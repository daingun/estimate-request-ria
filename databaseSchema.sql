-- Table structure for table `user`
CREATE TABLE `user` (
  `user_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `permissions` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
);

-- Table structure for table `product`
CREATE TABLE `product` (
  `product_code` PRIMARY KEY int(11) NOT NULL,
  `image` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
);

-- Table structure for table `options`
CREATE TABLE `options` (
  `option_code` PRIMARY KEY int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
);

-- Table structure for table `estimate`
CREATE TABLE `estimate` (
  `estimate_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `client` int(11) NOT NULL,
  `employee` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(6,2) unsigned DEFAULT NULL,
  UNIQUE KEY `estimate_id` (`estimate_id`,`product_id`),
  CONSTRAINT `estimate_ibfk_1` FOREIGN KEY (`client`)
    REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `estimate_ibfk_2` FOREIGN KEY (`employee`)
    REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `estimate_ibfk_3` FOREIGN KEY (`product_id`)
    REFERENCES `product` (`product_code`) ON UPDATE CASCADE
);

-- Table structure for table `prod_opt`
CREATE TABLE `prod_opt` (
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`option_id`),
  CONSTRAINT `prod_opt_ibfk_1` FOREIGN KEY (`product_id`)
    REFERENCES `product` (`product_code`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prod_opt_ibfk_2` FOREIGN KEY (`option_id`)
    REFERENCES `options` (`option_code`)
    ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `est_opt`
CREATE TABLE `est_opt` (
  `estimate_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`estimate_id`,`product_id`,`option_id`),
  CONSTRAINT `est_opt_ibfk_1` FOREIGN KEY (`estimate_id`, `product_id`)
    REFERENCES `estimate` (`estimate_id`, `product_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `est_opt_ibfk_2` FOREIGN KEY (`product_id`, `option_id`)
    REFERENCES `prod_opt` (`product_id`, `option_id`)
    ON DELETE CASCADE ON UPDATE CASCADE
);
