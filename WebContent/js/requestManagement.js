/**
 * Client estimate management
 */

(function() {
    let elc;
    let pc;
    //let sec;

    window.addEventListener("load", () => {
        if (sessionStorage.getItem("username") == null) {
            window.location.href = "index.html";
        } else {
            elc = new EstimateListController();
            elc.update();
            pc = new ProductController();
            pc.getProducts();
            new SendEstimateController(elc);
        }
    }, false);

    class EstimateListController {

        update() {
            serverRequest("GET", "GetClientEstimates", this.estimatesCallback.bind(this));
        }

        makeClientEstimateRow(estimate) {
            const row = document.createElement("tr");
            const idCell = document.createElement("td");
            idCell.textContent = estimate.id;
            row.appendChild(idCell);
            const nameCell = document.createElement("td");
            nameCell.textContent = estimate.product.name;
            row.appendChild(nameCell);
            const optionsCell = document.createElement("td");
            estimate.options.forEach((option) => {
                const sp = document.createElement("span");
                sp.textContent = option.name + " (" + option.type + ")";
                optionsCell.appendChild(sp);
            });
            row.appendChild(optionsCell);
            const priceCell = document.createElement("td");
            priceCell.textContent = estimate.price === undefined ? "to be priced" : estimate.price;
            row.appendChild(priceCell);

            return row;
        }

        fillClientEstimateTable(estimates) {
            const table = document.getElementById("clientEstimates");
            table.innerHTML = "";
            estimates.forEach((estimate) => {
                table.appendChild(this.makeClientEstimateRow(estimate));
            });
        }

        estimatesCallback(request) {
            if (request.readyState == XMLHttpRequest.DONE) {
                const message = request.responseText;
                switch (request.status) {
                    case 200:
                        const estimates = JSON.parse(message);
                        if (estimates.length == 0) {
                            document.getElementById("errorMsg").textContent = "No estimates yet!";
                            return;
                        } else {
                            this.fillClientEstimateTable(estimates);
                        }
                        break;
                    case 400: // Bad request.
                    case 401: // Unauthorized.
                    case 500: // Server error.
                        document.getElementById("errorMsg").textContent = message;
                        break;
                }
            }
        }
    }

    class ProductController {
        constructor() {
            this.productMap = new Map();
        }

        getProducts() {
            serverRequest("GET", "GetProducts", this.productsCallback.bind(this));
        }

        makeProductOption(product) {
            const option = document.createElement("option");
            option.value = product.code;
            option.textContent = product.name;
            return option;
        }

        displayProduct(list) {
            const p = this.productMap.get(list.value);
            document.getElementById("productImage").src = "./img/" + p.image;
            document.getElementById("productImage").alt = p.name;
            const po = document.getElementById("productOptions");
            po.innerHTML = "";
            p.options.forEach((option) => {
                const line = document.createElement("p");
                const cb = document.createElement("input");
                cb.type = "checkbox";
                cb.name = "selOpt";
                cb.value = option.code;
                line.appendChild(cb);
                const label = document.createElement("label");
                label.textContent = option.name + " " + option.type;
                line.appendChild(label);
                po.appendChild(line);
            });
        }

        fillProductsList(products) {
            const productList = document.getElementById("selectedProduct");
            products.forEach((product) => {
                productList.appendChild(this.makeProductOption(product));
                this.productMap.set(product.code.toString(), product);
            });
            productList.onchange = () => this.displayProduct(productList);
        }

        productsCallback(request) {
            if (request.readyState == XMLHttpRequest.DONE) {
                const message = request.responseText;
                switch (request.status) {
                    case 200:
                        const products = JSON.parse(message);
                        if (products.length == 0) {
                            document.getElementById("errorMsg").textContent = "No products yet!";
                            return;
                        } else {
                            this.fillProductsList(products);
                        }
                        break;
                    case 400: // Bad request.
                    case 401: // Unauthorized.
                    case 500: // Server error.
                        document.getElementById("errorMsg").textContent = message;
                        break;
                }
            }
        }
    }

    class SendEstimateController {
        constructor(estimateListController) {
            this.elc = estimateListController;
            this.addListener();
        }

        addListener() {
            document.getElementById("sendEstimate").addEventListener("click", (e) => {
                const form = e.target.closest("form");
                const checked = form.querySelectorAll("input[type='checkbox']:checked");
                if (checked.length === 0) {
                    document.getElementById("errorMsg").textContent = "Select al least one option";
                } else if (form.checkValidity()) {
                    document.getElementById("errorMsg").textContent = "";
                    serverRequest("POST", "SendEstimate", this.sendEstimateCallback.bind(this), form, true);
                    this.reset();
                } else {
                    form.reportValidity();
                }
            });
        }

        sendEstimateCallback(request) {
            if (request.readyState == XMLHttpRequest.DONE) {
                const message = request.responseText;
                switch (request.status) {
                    case 200:
                        this.elc.update();
                        break;
                    case 400: // Bad request.
                    case 401: // Unauthorized.
                    case 500: // Server error.
                        document.getElementById("errorMsg").textContent = message;
                        break;
                }
            }
        }

        reset() {
            document.getElementById("productOptions").innerHTML = "";
            document.getElementById("productImage").src = "";
            document.getElementById("productImage").alt = "";
        }
    }
})();
