/**
 * 
 */

(function() {
    let ec;
    let pdc;

    window.addEventListener("load", () => {
        if (sessionStorage.getItem("username") == null) {
            window.location.href = "index.html";
        } else {
            pdc = new PriceDetailsController();
            ec = new EstimatesController(pdc);
            ec.update();
            new SendPriceController(ec);
        }
    }, false);

    class EstimatesController {

        constructor(priceDetailsController) {
            this.pdc = priceDetailsController;
            this.estimateMap = new Map();
        }

        update() {
            serverRequest("GET", "GetEmployeeEstimates",
                this.estimatesCallback.bind(this));
        }

        estimatesCallback(request) {
            if (request.readyState == XMLHttpRequest.DONE) {
                const message = request.responseText;
                switch (request.status) {
                    case 200:
                        const estimates = JSON.parse(message);
                        if (estimates.length == 0) {
                            document.getElementById("errorMsg").textContent = "No estimates yet!";
                            return;
                        } else {
                            this.fillEmployeeEstimateTable(estimates);
                        }
                        break;
                    case 400: // Bad request.
                    case 401: // Unauthorized.
                    case 500: // Server error.
                        document.getElementById("errorMsg").textContent = message;
                        break;
                }
            }
        }

        fillEmployeeEstimateTable(estimates) {
            const unassignedTable = document.getElementById("unassignedEstimates");
            unassignedTable.innerHTML = "";
            const assignedTable = document.getElementById("myEstimates");
            assignedTable.innerHTML = "";
            estimates.forEach((estimate) => {
                if (estimate.price === undefined) {
                    unassignedTable.appendChild(this.makeEstimateRow(estimate));
                    this.estimateMap.set(estimate.id.toString(), estimate);
                } else {
                    assignedTable.appendChild(this.makeEstimateRow(estimate));
                }
            });
        }

        makeEstimateRow(estimate) {
            const row = document.createElement("tr");
            const idCell = document.createElement("td");
            idCell.textContent = estimate.id;
            row.appendChild(idCell);
            const nameCell = document.createElement("td");
            nameCell.textContent = estimate.product.name;
            row.appendChild(nameCell);
            const optionsCell = document.createElement("td");
            estimate.options.forEach((option) => {
                const sp = document.createElement("span");
                sp.textContent = option.name + " (" + option.type + ")";
                optionsCell.appendChild(sp);
            });
            row.appendChild(optionsCell);
            const priceCell = document.createElement("td");
            if (estimate.price === undefined) {
                const a = document.createElement("a");
                a.href = "#";
                a.textContent = "Set price";
                a.onclick = () => this.estimateDetails(estimate.id);
                priceCell.appendChild(a);
            } else {
                priceCell.textContent = estimate.price;
            }
            row.appendChild(priceCell);

            return row;
        }

        estimateDetails(estimateId) {
            this.pdc.setDetail(this.estimateMap.get(estimateId.toString()));
            document.getElementById("estimatePriceForm").style.display = "block";
        }
    }

    class PriceDetailsController {

        setDetail(estimate) {
            document.getElementById("estimateId").textContent = estimate.id;
            document.getElementById("clientData").textContent = estimate.client.surname +
                " " + estimate.client.name + " (" + estimate.client.username + ")";
            document.getElementById("productData").textContent = estimate.product.name +
                " (" + estimate.product.code + ")";
            const opts = document.getElementById("optionsData");
            estimate.options.forEach((option) => {
                const sp = document.createElement("span");
                sp.textContent = option.name + " (" + option.type + ")";
                opts.appendChild(sp);
            });
            document.getElementById("productImage").src = "./img/" + estimate.product.image;
            document.getElementById("estId").value = estimate.id;
        }
    }

    class SendPriceController {

        constructor(estimateController) {
            this.ec = estimateController;
            this.addListener();
        }

        addListener() {
            document.getElementById("setPrice").addEventListener("click", (e) => {
                const form = e.target.closest("form");
                const price = document.getElementById("price");
                const err = document.getElementById("errorMsg");
                if (price.value < 0.01 || price.value > 9999.99) {
                    err.textContent = "Price must be between 0.01 and 9999.99";
                } else if (form.checkValidity()) {
                    err.textContent = "";
                    serverRequest("POST", "SetPrice", this.sendPriceCallback.bind(this), form, true);
                } else {
                    form.reportValidity();
                }
            });
        }

        sendPriceCallback(request) {
            if (request.readyState == XMLHttpRequest.DONE) {
                const message = request.responseText;
                switch (request.status) {
                    case 200:
                        this.ec.update();
                        document.getElementById("estimatePriceForm").style.display = "none";
                        this.reset();
                        break;
                    case 400: // Bad request.
                    case 401: // Unauthorized.
                    case 500: // Server error.
                        document.getElementById("errorMsg").textContent = message;
                        break;
                }
            }
        }

        reset() {
            document.getElementById("estId").value = "";
            document.getElementById("price").value = "";
        }
    }
})();
