(function() {
    document.getElementById("loginButton").addEventListener("click", (e) => {
        const form = e.target.closest("form");
        if (form.checkValidity()) {
            serverRequest("POST", "Login", loginCallback, form, true);
        } else {
            form.reportValidity();
        }
    });

    function loginCallback(request) {
        if (request.readyState == XMLHttpRequest.DONE) {
            const message = request.responseText;
            const err = document.getElementById("errorMsg");
            switch (request.status) {
                case 200:
                    sessionStorage.setItem("username", message);
                    const user = JSON.parse(message);
                    if (user.permission == "client") {
                        window.location.href = "client.html";
                    }
                    else if (user.permission == "employee") {
                        window.location.href = "employee.html";
                    } else {
                        err.textContent = "User cannot be authenticated";
                    }
                    break;
                case 400: // Bad request.
                case 401: // Unauthorized.
                case 500: // Server error.
                    err.textContent = message;
                    break;
            }
        }
    }

    document.getElementById("registerButton").addEventListener("click", (e) => {
        const form = e.target.closest("form");
        const pwd = document.getElementById("regPwd");
        const pwd2 = document.getElementById("regPwd2");
        const err = document.getElementById("registerError");
        if (pwd.value !== pwd2.value) {
            err.textContent = "The two passwords must be the same";
        }
        else if (form.checkValidity()) {
            serverRequest("POST", "Register", registerCallback, form, true);
        } else {
            form.reportValidity();
        }
    });

    function registerCallback(request) {
        if (request.readyState == XMLHttpRequest.DONE) {
            const message = request.responseText;
            const err = document.getElementById("registerError");
            switch (request.status) {
                case 200:
                    alert("User has been registerd, please log in");
                    break;
                case 400: // Bad request.
                case 401: // Unauthorized.
                case 500: // Server error.
                    err.textContent = message;
                    break;
            }
        }
    }
})();
