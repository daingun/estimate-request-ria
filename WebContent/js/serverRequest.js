/**
 * Server call management
 */

function serverRequest(method, url, callback, formElement = null, reset = true) {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        callback(request);
    };
    request.open(method, url);
    if (formElement == null) {
        request.send();
    } else {
        const data = new FormData(formElement);
        request.send(data);
    }
    if (formElement !== null && reset) {
        formElement.reset();
    }
}