package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import it.polimi.tiw.estimaterequest.dao.UserDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
@MultipartConfig
public class Register extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
    }

    /**
     * @see Servlet#init()
     */
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
    }

    /**
     * @see Servlet#destroy()
     */
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = StringEscapeUtils.escapeJava(request.getParameter("regUser"));
        String name = StringEscapeUtils.escapeJava(request.getParameter("regName"));
        String surname = StringEscapeUtils.escapeJava(request.getParameter("regSurname"));
        String email = StringEscapeUtils.escapeJava(request.getParameter("regEmail"));
        String password = StringEscapeUtils.escapeJava(request.getParameter("regPwd"));
        String password2 = StringEscapeUtils.escapeJava(request.getParameter("regPwd2"));
        String permissions = StringEscapeUtils.escapeJava(request.getParameter("regPermission"));
        if (username == null || name == null || surname == null || email == null || password == null
                || password2 == null || permissions == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Missing registrations values");
            return;
        }
        if (!isValidEmail(email)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Invalid email address");
            return;
        }
        if (!password.equals(password2)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Missing registrations values");
            return;
        }
        if (!permissions.equals("client") && !permissions.equals("employee")) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Inconsistent inputs");
            return;
        }

        UserDAO userDAO = new UserDAO(connection);
        try {
            userDAO.registerUser(username, name, surname, email, password, permissions);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (SQLException e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().println("Not Possible to register user");
            return;
        }
    }

    // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/email#Validation
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");

    /**
     * Verify the email address
     * 
     * @param email - email to be verified
     * @return `true` if the email address is valid
     */
    private boolean isValidEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }

}
