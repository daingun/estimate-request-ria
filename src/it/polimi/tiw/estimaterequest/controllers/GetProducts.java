package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.estimaterequest.beans.Option;
import it.polimi.tiw.estimaterequest.beans.Product;
import it.polimi.tiw.estimaterequest.dao.OptionDAO;
import it.polimi.tiw.estimaterequest.dao.ProductDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;

/**
 * Servlet implementation class GetProducts
 */
@WebServlet("/GetProducts")
public class GetProducts extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetProducts() {
        super();
    }

    /**
     * @see Servlet#init()
     */
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
    }

    /**
     * @see Servlet#destroy()
     */
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO(connection);
        OptionDAO optionDAO = new OptionDAO(connection);
        List<Product> allProducts;
        try {
            allProducts = productDAO.getAllProducts();
            for (Product product : allProducts) {
                List<Option> options = optionDAO.getProductOptions(product.getCode());
                product.setOptions(options);
            }
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(allProducts);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
            return;
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().println("Not possible to recover products");
            return;
        }
    }

}
