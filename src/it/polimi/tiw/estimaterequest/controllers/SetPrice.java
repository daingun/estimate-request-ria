package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.tiw.estimaterequest.beans.User;
import it.polimi.tiw.estimaterequest.dao.EstimateDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;

/**
 * Servlet implementation class SetPrice
 */
@WebServlet("/SetPrice")
@MultipartConfig
public class SetPrice extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;

    public SetPrice() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int estimateId;
        BigDecimal price;
        try {
            estimateId = Integer.parseInt(request.getParameter("estId"));
            price = new BigDecimal(request.getParameter("price"));
        } catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Estimate does not exists");
            return;
        }
        boolean small = price.compareTo(BigDecimal.valueOf(0.01)) < 0;
        boolean large = price.compareTo(BigDecimal.valueOf(9999.99)) > 0;
        boolean scale = price.scale() > 2;
        if (small || large || scale) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Price out of range");
            return;
        }

        User employee = (User) session.getAttribute("user");
        EstimateDAO estimateDAO = new EstimateDAO(connection);
        try {
            estimateDAO.setEstimatePrice(price, estimateId, employee.getId());
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().println("Not possible to set the price of the estimate");
            return;
        }
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
