package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.estimaterequest.beans.FullEstimate;
import it.polimi.tiw.estimaterequest.beans.User;
import it.polimi.tiw.estimaterequest.dao.EstimateDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;

/**
 * Servlet implementation class EmployeeHome
 */
@WebServlet("/GetEmployeeEstimates")
public class GetEmployeeEstimates extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;

    public GetEmployeeEstimates() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User employee = (User) session.getAttribute("user");
        EstimateDAO estimateDAO = new EstimateDAO(connection);
        List<FullEstimate> estimates = new ArrayList<FullEstimate>();
        try {
            estimates.addAll(estimateDAO.getFullEstimateUnassigned());
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().println("Not possible to recover unassigned estimates");
            return;
        }

        try {
            estimates.addAll(estimateDAO.getFullEstimateByEmployeeId(employee.getId()));
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().println("Not possible to recover employee estimates");
            return;
        }

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(estimates);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
