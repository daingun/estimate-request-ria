package it.polimi.tiw.estimaterequest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.polimi.tiw.estimaterequest.beans.Product;

public class ProductDAO {
    /**
     * Database connection.
     */
    private Connection connection;

    /**
     * Create a new object to access the product data from the database.
     * 
     * @param connection - Database connection.
     */
    public ProductDAO(Connection connection) {
        this.connection = connection;
    }

    public Optional<Product> getProductByCode(int productCode) throws SQLException {
        String query = "SELECT * FROM product WHERE product_code = ?";
        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, productCode);
            try (ResultSet result = ps.executeQuery();) {
                if (!result.isBeforeFirst()) {
                    return Optional.empty();
                } else {
                    result.next();
                    Product product = new Product();
                    product.setCode(result.getInt("product_code"));
                    product.setImage(result.getString("image"));
                    product.setName(result.getString("name"));
                    return Optional.of(product);
                }
            }
        }
    }

    /**
     * Get all the available products.
     * 
     * @return A list of product.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<Product> getAllProducts() throws SQLException {
        List<Product> products = new ArrayList<>();
        String query = "SELECT * FROM product";
        try (PreparedStatement ps = connection.prepareStatement(query);) {
            try (ResultSet result = ps.executeQuery();) {
                while (result.next()) {
                    Product product = new Product();
                    product.setCode(result.getInt("product_code"));
                    product.setImage(result.getString("image"));
                    product.setName(result.getString("name"));
                    products.add(product);
                }
            }
        }
        return products;
    }
}
