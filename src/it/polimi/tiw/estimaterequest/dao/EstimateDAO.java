package it.polimi.tiw.estimaterequest.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.polimi.tiw.estimaterequest.beans.FullEstimate;
import it.polimi.tiw.estimaterequest.beans.Option;
import it.polimi.tiw.estimaterequest.beans.Product;
import it.polimi.tiw.estimaterequest.beans.User;

public class EstimateDAO {
    /**
     * Database connection.
     */
    private Connection connection;

    /**
     * Create a new object to access the product data from the database.
     * 
     * @param connection - Database connection.
     */
    public EstimateDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Save the estimated created by a client. Insert into the database the
     * association between the given estimate, product and option.
     * 
     * @param clientId    - Client id number.
     * @param productCode - Product code number.
     * @param options     - List of option codes to be added for the given estimate
     *                    and product.
     * @return
     * @throws SQLException
     */
    public int sendEstimate(int clientId, int productCode, String[] options) throws SQLException {
        String insertEst = "INSERT INTO estimate(client, product_id) VALUES(?, ?)";
        // TODO https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-usagenotes-last-insert-id.html
        String select = "SELECT LAST_INSERT_ID() AS last_id";
        String insertOpt = "INSERT INTO est_opt(estimate_id, product_id, option_id) VALUES(?, ?, ?)";

        connection.setAutoCommit(false);
        PreparedStatement psInsertEst = null;
        PreparedStatement psSelect = null;
        PreparedStatement psInsertOpt = null;
        ResultSet result = null;
        try {
            psInsertEst = connection.prepareStatement(insertEst);
            psSelect = connection.prepareStatement(select);
            psInsertEst.setInt(1, clientId);
            psInsertEst.setInt(2, productCode);
            psInsertEst.executeUpdate();
            int insertedEstimateId;
            result = psSelect.executeQuery();
            if (!result.isBeforeFirst()) {
                insertedEstimateId = 0;
            } else {
                result.next();
                insertedEstimateId = result.getInt("last_id");
            }
            psInsertOpt = connection.prepareStatement(insertOpt);
            for (String option : options) {
                psInsertOpt.setInt(1, insertedEstimateId);
                psInsertOpt.setInt(2, productCode);
                int optionCode = Integer.parseInt(option);
                psInsertOpt.setInt(3, optionCode);
                psInsertOpt.executeUpdate();
            }
            connection.commit();
            return insertedEstimateId;
        } catch (SQLException e) {
            if (connection != null) {
                connection.rollback();
            }
            throw e;
        } finally {
            if (result != null) {
                result.close();
            }
            if (psInsertOpt != null) {
                psInsertOpt.close();
            }
            if (psSelect != null) {
                psSelect.close();
            }
            if (psInsertEst != null) {
                psInsertEst.close();
            }
            connection.setAutoCommit(true);
        }
    }

    /**
     * Set the price of the given estimate.
     * 
     * @param price      - Price of the estimate.
     * @param employeeId - Employee id number.
     * @param estimateId - Estimate id number.
     * @throws SQLException - If the internal SQL query fails.
     */
    public void setEstimatePrice(BigDecimal price, int estimateId, int employeeId)
            throws SQLException {
        String update = "UPDATE estimate SET price = ?, employee = ? WHERE estimate_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(update);) {
            ps.setBigDecimal(1, price);
            ps.setInt(2, employeeId);
            ps.setInt(3, estimateId);
            ps.executeUpdate();
        }
    }

    /**
     * Get the estimate by its id number.
     * 
     * @param estimateId - Estimate id number.
     * @return - Estimate.
     * @throws SQLException - If the internal SQL query fails.
     */
    public Optional<FullEstimate> getFullEstimateById(int estimateId) throws SQLException {
        List<FullEstimate> fullEstimates = new ArrayList<>();
        String query = FULL_ESTIMATE_SELECT + " WHERE estimate_id = ?";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, estimateId);
            extracted(fullEstimates, ps);
        }
        if (fullEstimates.size() == 1) {
            return Optional.of(fullEstimates.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the list of estimates not assigned to an employee.
     * 
     * @return List of estimates.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<FullEstimate> getFullEstimateUnassigned() throws SQLException {
        List<FullEstimate> fullEstimates = new ArrayList<>();
        String query = FULL_ESTIMATE_SELECT + " WHERE employees.user_id IS NULL";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            extracted(fullEstimates, ps);
        }
        return fullEstimates;
    }

    /**
     * Get the list of the estimates created by the given client.
     * 
     * @param clientId - Client id number.
     * @return List of estimates.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<FullEstimate> getFullEstimateByClientId(int clientId) throws SQLException {
        List<FullEstimate> fullEstimates = new ArrayList<>();
        String query = FULL_ESTIMATE_SELECT + " WHERE clients.user_id = ?";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, clientId);
            extracted(fullEstimates, ps);
        }
        return fullEstimates;
    }

    /**
     * Get a list of estimates associated with the given employee.
     * 
     * @param employeeId - Employee id number.
     * @return List of estimates.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<FullEstimate> getFullEstimateByEmployeeId(int employeeId) throws SQLException {
        List<FullEstimate> fullEstimates = new ArrayList<>();
        String query = FULL_ESTIMATE_SELECT + " WHERE employees.user_id = ?";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, employeeId);
            extracted(fullEstimates, ps);
        }
        return fullEstimates;
    }

    private static final String FULL_ESTIMATE_SELECT = "SELECT estimate.estimate_id,"
            + " estimate.price, clients.user_id, clients.username, clients.name,"
            + " clients.surname, product.product_code, product.image, product.name,"
            + " employees.user_id, employees.username, employees.name, employees.surname"
            + " FROM estimate LEFT OUTER JOIN"
            + " user AS employees ON estimate.employee = employees.user_id"
            + " JOIN product ON estimate.product_id = product.product_code"
            + " JOIN user AS clients ON estimate.client = clients.user_id";

    private void extracted(List<FullEstimate> fullEstimates, PreparedStatement ps)
            throws SQLException {
        try (ResultSet result = ps.executeQuery();) {
            OptionDAO optionDAO = new OptionDAO(connection);
            while (result.next()) {
                FullEstimate estimate = new FullEstimate();
                int estimateId = result.getInt("estimate.estimate_id");
                estimate.setId(estimateId);
                estimate.setPrice(result.getBigDecimal("estimate.price"));
                User client = new User();
                client.setId(result.getInt("clients.user_id"));
                client.setUsername(result.getString("clients.username"));
                client.setName(result.getString("clients.name"));
                client.setSurname(result.getString("clients.surname"));
                estimate.setClient(client);
                User employee = new User();
                employee.setId(result.getInt("employees.user_id"));
                employee.setUsername(result.getString("employees.username"));
                employee.setName(result.getString("employees.name"));
                employee.setSurname(result.getString("employees.surname"));
                estimate.setEmployee(employee);
                Product product = new Product();
                product.setCode(result.getInt("product.product_code"));
                product.setImage(result.getString("product.image"));
                product.setName(result.getString("product.name"));
                estimate.setProduct(product);
                List<Option> options = optionDAO.getEstimateOptions(estimateId);
                estimate.setOptions(options);
                fullEstimates.add(estimate);
            }
        }
    }
}
