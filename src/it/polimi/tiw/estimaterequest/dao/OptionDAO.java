package it.polimi.tiw.estimaterequest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.tiw.estimaterequest.beans.Option;

public class OptionDAO {
    /**
     * Database connection.
     */
    private Connection connection;

    /**
     * Create a new object to access the option data from the database.
     * 
     * @param connection - Database connection.
     */
    public OptionDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Get the list of options available for the given product
     * 
     * @param productCode - Product code number.
     * @return List of options.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<Option> getProductOptions(int productCode) throws SQLException {
        List<Option> options = new ArrayList<>();
        String query = "SELECT option_code, type, name"
                + " FROM prod_opt JOIN options ON prod_opt.option_id = options.option_code"
                + " WHERE product_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, productCode);
            try (ResultSet result = ps.executeQuery();) {
                while (result.next()) {
                    Option option = new Option();
                    option.setCode(result.getInt("option_code"));
                    option.setType(result.getString("type"));
                    option.setName(result.getString("name"));
                    options.add(option);
                }
            }
        }
        return options;
    }

    /**
     * Get the list of options associated to the given estimate.
     * 
     * @param estimateId - Estimate id number.
     * @return List of options.
     * @throws SQLException - If the internal SQL query fails.
     */
    public List<Option> getEstimateOptions(int estimateId) throws SQLException {
        List<Option> options = new ArrayList<>();
        String query = "SELECT option_code, type, name"
                + " FROM est_opt JOIN options ON est_opt.option_id = options.option_code"
                + " WHERE estimate_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, estimateId);
            try (ResultSet result = ps.executeQuery();) {
                while (result.next()) {
                    Option option = new Option();
                    option.setCode(result.getInt("option_code"));
                    option.setType(result.getString("type"));
                    option.setName(result.getString("name"));
                    options.add(option);
                }
            }
        }
        return options;
    }

}
