# Estimate Request - Rich Internet Application

Web application to manage the estimate requests for customized products.

The application has a fat client, it uses javascript for asynchronous request to the server, web page modification is done client side, the server sends data in JSON format.

The project contains the web application, the database schema and the software design document.
